# Rhino Linux Scripts


## Getting started

This scripts is meant to make life easier when changing desktop enviroments from Unicorn to either Gnome or KDE. 

Remember you using this at your own risk. This is not an official script please don't ask Rhino Linux team for help. 

## Steps Rhino Linux (64 bit)

Open up a terminal (be root) and run 

- git clone https://gitlab.com/gosh-its-arch-linux/rhino-linux.git
- cd rhino-linux
- chmod +x *.sh
- sudo ./desktop_chooser.sh


Once completed you should be able to run the individual script

## What the file does

- desktop_chooser -> Use this script to choose between installing Ubuntu Desktop (Gnome) OR Kubuntu Desktop (KDE)

## Steps Rhino Linux - GUI (64 bit)

Does the same as the command line script but has a basic GUI you can click through. 

NB -> Please make sure Zenity is installed on your system (Before using the GUI script):

- sudo nala install zenity

Open up a terminal (be root) and run

- git clone https://gitlab.com/gosh-its-arch-linux/rhino-linux.git
- cd rhino-linux
- cd GUI
- chmod +x *.sh
- sudo ./gui_interface.sh
