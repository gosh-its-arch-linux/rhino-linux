#!/bin/bash

# Print a welcome message
zenity --info --title="Welcome" --text="Welcome to Gosh-Its-Arch Rhino-Rolling Desktop Changer"
sleep 2

# Exit script if any command fails
set -e

# First Step: Update rhino-pkg
zenity --info --title="Update" --text="Updating rhino-pkg..."
rhino-pkg update -y
sleep 2

# Ask user which desktop environment to install
choice=$(zenity --list --title="Desktop Environment Selection" --column="Options" "Ubuntu Desktop" "Kubuntu Desktop")

case $choice in
    "Ubuntu Desktop")
        zenity --info --title="Installation" --text="Installing ubuntu-desktop...\nNOTE: When prompted, please select the 'gdm' option!"
        sudo nala install ubuntu-desktop -y
        ;;
    "Kubuntu Desktop")
        zenity --info --title="Installation" --text="Installing kubuntu-desktop..."
        sudo nala install kubuntu-desktop -y
        
        # Automatically install dolphin for Kubuntu Desktop users
        zenity --info --title="Installation" --text="Installing dolphin file manager..."
        sudo nala install dolphin -y
        sleep 2

        # Automatically install konsole for Kubuntu Desktop users
        zenity --info --title="Installation" --text="Installing konsole terminal..."
        sudo nala install konsole -y
        sleep 2
        ;;
    *)
        zenity --error --title="Error" --text="Invalid choice! Exiting..."
        exit 1
        ;;
esac

zenity --info --title="Success" --text="All tasks completed successfully.\nPlease reboot your system and select your new desktop environment on the login screen."
