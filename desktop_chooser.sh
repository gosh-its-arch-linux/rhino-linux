#!/bin/bash

# Print a welcome message
echo "Welcome to Gosh-Its-Arch Rhino-Rolling Desktop Changer"
sleep 3

# Exit script if any command fails
set -e

# First Step: Update rhino-pkg
echo "Updating rhino-pkg..."
rhino-pkg update -y
sleep 3

# Ask user which desktop environment to install
echo "Which desktop environment would you like to install?"
echo "1) Ubuntu Desktop"
echo "2) Kubuntu Desktop"
read -p "Enter your choice (1/2): " choice

case $choice in
    1)
        echo "Installing ubuntu-desktop..."
        echo "NOTE: When prompted, please select the 'gdm' option!"
        sudo nala install ubuntu-desktop -y
        ;;
    2)
        echo "Installing kubuntu-desktop..."
        sudo nala install kubuntu-desktop -y
        
        # Automatically install dolphin for Kubuntu Desktop users
        echo "Installing dolphin file manager..."
        sudo nala install dolphin -y
        sleep 3

        # Automatically install konsole for Kubuntu Desktop users
        echo "Installing konsole terminal..."
        sudo nala install konsole -y
        sleep 3
        ;;
    *)
        echo "Invalid choice! Exiting..."
        exit 1
        ;;
esac

echo "All tasks completed successfully."
echo "Please reboot your system and select your new desktop environment on the login screen."
